""" This module contains clingo interaction functions """
from __future__ import print_function

import subprocess
#from subprocess import CalledProcessError
import sys, os
import numpy as np
from gunfolds.tools import bfutils as bfu
import json

CLINGOPATH=''
CAPSIZE=1000


def rate(u):
    s = "u("+str(u)+")."
    return s

def gen_program():
    s = """
u(1..3).
directed(X,Y,1) :- edge(X,Y).
directed(X,Y,L) :- directed(X,Z,L-1), edge(Z,Y), L <= U, u(U).
bidirected(X,Y,U) :- directed(Z,X,L), directed(Z,Y,L), node(X), node(Y), node(Z), X < Y, L < U, u(U).
#show directed/3.
#show bidirected/3.
#show graph/1.
    """
    return s
# :- directed(X,Y,L), directed(Z,K,M), node(X;Y;Z;K), M != L.

def uprogram():
    s = """
{ edge1(X,Y) } :- node(X),node(Y).
edge(X,Y,1) :- edge1(X,Y).
edge(X,Y,L) :- edge(X,Z,L-1),edge1(Z,Y), L <= U, u(U).
derived_edgeu(X,Y) :- edge(X,Y,L), u(L).
derived_confu(X,Y) :- edge(Z,X,L), edge(Z,Y,L),node(X),node(Y),node(Z),X < Y, L < U, u(U).
:- edgeu(X,Y), not derived_edgeu(X,Y),node(X),node(Y).
:- not edgeu(X,Y), derived_edgeu(X,Y),node(X),node(Y).
:- derived_confu(X,Y), not confu(X,Y),node(X),node(Y), X < Y.
:- not derived_confu(X,Y), confu(X,Y),node(X),node(Y), X < Y.
    """
    return s

def wuprogram():
    s = """
    { edge1(X,Y) } :- node(X), node(Y).
    path(X,Y,1) :- edge1(X,Y).
    path(X,Y,L) :- path(X,Z,L-1), edge1(Z,Y), L <= U, u(U).
    edgeu(X,Y) :- path(X,Y,L), u(L).
    confu(X,Y) :- path(Z,X,L), path(Z,Y,L), node(X;Y;Z), X < Y, L < U, u(U).
    :~ edgeh(X,Y,W), not edgeu(X,Y). [W,X,Y,1]
    :~ no_edgeh(X,Y,W), edgeu(X,Y). [W,X,Y,1]
    :~ confh(X,Y,W), not confu(X,Y). [W,X,Y,2]
    :~ no_confh(X,Y,W), confu(X,Y). [W,X,Y,2]
    """
    return s

def g2wclingo(g):
    """ Convert a graph to a string of grounded terms for clingo """
    s = ''
    n = len(g)
    s += 'node(1..'+str(n)+'). '
    for v in range(1,n+1):
        for w in range(1,n+1):
            if w in g[v]:
                if g[v][w] == 1:
                    s += 'edgeh('+str(v)+','+str(w)+',1). '
                    s += 'no_confh('+str(v)+','+str(w)+',1). '
                if g[v][w] == 2:
                    s += 'confh('+str(v)+','+str(w)+',1). '
                    s += 'no_edgeh('+str(v)+','+str(w)+',1). '
                if g[v][w] == 3:
                    s += 'edgeh('+str(v)+','+str(w)+',1). '
                    s += 'confh('+str(v)+','+str(w)+',1). '
            else:
                s += 'no_confh('+str(v)+','+str(w)+',1). '
                s += 'no_edgeh('+str(v)+','+str(w)+',1). '
    return s

def baseg2clingo(g):
    """ Convert a graph to a string of grounded terms for clingo """
    s = ''
    n = len(g)
    s += 'node(1..'+str(n)+'). '
    for v in g:
        for w in g[v]:
            if g[v][w] == 1: s += 'edge('+str(v)+','+str(w)+'). '
            if g[v][w] != 1:
                raise ValueError('Input graph is not at rate 1')
    return s

def g2clingo(g):
    """ Convert a graph to a string of grounded terms for clingo """
    s = ''
    n = len(g)
    s += 'node(1..'+str(n)+'). '
    for v in g:
        for w in g[v]:
            if g[v][w] == 1: s += 'edgeu('+str(v)+','+str(w)+'). '
            if g[v][w] == 2: s += 'confu('+str(v)+','+str(w)+'). '
            if g[v][w] == 3:
                s += 'edgeu('+str(v)+','+str(w)+'). '
                s += 'confu('+str(v)+','+str(w)+'). '
    return s


def clingo_high_version(cpath=CLINGOPATH):
    v = os.popen(cpath+"clingo --version").read()[15:20]
    #v = subprocess.check_output(['clingo', '--version'])[15:20]
    return int(v.split('.')[1]) >= 5

def weqclass(g,
            urate=2,
            timeout=0,
            capsize=CAPSIZE,
            cpath=CLINGOPATH):

    try:
        if clingo_high_version(cpath=cpath):
            clg_start = 'clingo -W no-atom-undefined '
        else:
            clg_start = 'clingo --Wno-atom-undefined '
    except:
        return {}
    exp_result = 'OPTIMUM FOUND'
    clingo_command = cpath+clg_start+'--outf=2 --opt-mode=opt --time-limit='+str(timeout)\
      +' -n '+str(capsize)+' '

    try:
        p = subprocess.Popen(clingo_command.split(),
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             close_fds=True)
    except:
        return {}

    command = g2wclingo(g) + ' ' + rate(urate) + ' ' + wuprogram()
    command = command.encode().replace("\n", " ")
    (output, err) = p.communicate(command)

    if not err:
        result = json.loads(output.decode())
    else:
        if err[:8] != '*** Warn':
            print(err)
            return {}
        else:
            result = json.loads(output.decode())
    if result['Result'] == exp_result:
        r = jclingo2g(result['Call'][0]['Witnesses'][-1]['Value'])
        return r

    return {}


def eqclass(g,
            urate=2,
            exact=True,
            timeout=0,
            capsize=CAPSIZE,
            cpath=CLINGOPATH):

    try:
        clg_start = 'clingo -W no-atom-undefined '
        # if clingo_high_version(cpath=cpath):
        #     clg_start = 'clingo -W no-atom-undefined '
        # else:
        #     clg_start = 'clingo --Wno-atom-undefined '
    except:
        return {}
    if exact:
        exp_result = 'SATISFIABLE'
        clingo_command = cpath+clg_start+'--outf=2 --time-limit='+str(timeout)\
          +' -n '+str(capsize)+' '

    else:
        exp_result = 'OPTIMUM FOUND'
        clingo_command = cpath+clg_start+'--outf=2 --opt-mode=opt --time-limit='+str(timeout)\
          +' -n '+str(capsize)+' '

    try:
        p = subprocess.Popen(clingo_command.split(),
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             close_fds=True)
    except:
        return {}

    if exact:
        command = g2clingo(g) + ' ' + rate(urate) + ' ' + uprogram()
    else:
        command = g2wclingo(g) + ' ' + rate(urate) + ' ' + wuprogram()
    command = command.encode().replace(b"\n", b" ")
    (output, err) = p.communicate(command)

    if not err:
        result = json.loads(output.decode())
    else:
        if err[:8] != '*** Warn':
            print(err)
            return {}
        else:
            result = json.loads(output.decode())
    if result['Result'] == exp_result:
        if exact:
         r = {jclingo2g(value['Value']) for value in result['Call'][0]['Witnesses']}
        else:
            r = jclingo2g(result['Call'][0]['Witnesses'][-1]['Value'])
        return r

    return {}

def rasl(g,
         exact=True,
         timeout=0,
         capsize=CAPSIZE,
         cpath=CLINGOPATH):

    try:
        clg_start = 'clingo -W no-atom-undefined '
    except:
        return {}
    if exact:
        exp_result = 'SATISFIABLE'
        clingo_command = cpath+clg_start+'--outf=2 --time-limit='+str(timeout)\
          +' -n '+str(capsize)+' '

    else:
        exp_result = 'OPTIMUM FOUND'
        clingo_command = cpath+clg_start+'--outf=2 --opt-mode=opt --time-limit='+str(timeout)\
          +' -n '+str(capsize)+' '

    try:
        p = subprocess.Popen(clingo_command.split(),
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             close_fds=True)
    except:
        return {}

    command = baseg2clingo(g) + ' ' + gen_program()
    command = command.encode().replace(b"\n", b" ")
    (output, err) = p.communicate(command)
    print(output)

    if not err:
        result = json.loads(output.decode())
    else:
        if err[:8] != '*** Warn':
            print(err)
            return {}
        else:
            result = json.loads(output.decode())
    if result['Result'] == exp_result:
        print(result['Call'][0]['Witnesses'])
        #r = {jclingo2g(value['Value']) for value in result['Call'][0]['Witnesses']}
        return result['Call'][0]['Witnesses']

    return {}


def clingo2num(value):
    a2edgetuple(x)


def a2edgetuple(answer):
    edges = [x for x in answer if 'edge1' in x]
    u = [x for x in answer if x[0]=='u'][0]
    return edges,u


def c2edgepairs(clist):
    return [x[6:-1].split(',') for x in clist]


def nodenum(edgepairs):
    nodes = 0
    for e in edgepairs:
        nodes = np.max([nodes, np.max([int(_) for _ in e])])
    return nodes

def edgepairs2g(edgepairs):
    n = nodenum(edgepairs)
    g = {x+1:{} for x in range(n)}
    for e in edgepairs:
        g[int(e[0])][int(e[1])] = 1
    return g


def jclingo2g(output_g):
    l = a2edgetuple(output_g)
    l = (c2edgepairs(l[0]),l[1])
    l = (bfu.g2num(edgepairs2g(l[0])),int(l[1][2:-1]))
    return l
