gunfolds
========

Tools to explore dynamic causal graphs in the case of  undersampled data helping to unfold the apparent structure into the underlying truth.

Acknowledgment
========
This work was supported by  NSF IIS-1318759 grant.
